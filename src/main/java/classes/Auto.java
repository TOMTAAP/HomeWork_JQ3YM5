package classes;

/*
 T�th M�t�
 TOMTAAP.PTE/ JQ3YM5
 tmate9105@gmail.com 
 2018.05.07
 */

public class Auto extends Jarmu {
	private String uzemanyag;	
	
	public Auto(String nev, String tipus,String marka,String uzemanyag,int fogyasztas) {
		super(nev,tipus,marka,fogyasztas);
		this.uzemanyag=uzemanyag;
	}
	public String getUzemanyag() {
		return uzemanyag;
	}

	public void setUzemanyag(String uzemanyag) {
		this.uzemanyag = uzemanyag;
	}
	

	@Override
	public String hatra() {	
		return "ide�lis esetben van h�trament fokozat"; 
		/*Sajnos a fant�zi�m el�g szeg�nyes, igy mivel az id� sz�k�ben vagyok nem tudtam jobbat kital�lni.
			Tudom hogy ennek igy sajnos nem sok �rtelme van, de annyit bemutat hogy kell egy �r�k�lt met�dust overrideolni.
		 */	
	}
	
	@Override
	public String toString() {
		return super.toString()+" Motort�pus: " +uzemanyag;
	}
	

}
