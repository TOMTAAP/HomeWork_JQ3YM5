package classes;
/*
T�th M�t�
TOMTAAP.PTE/ JQ3YM5
tmate9105@gmail.com 
2018.05.07
*/

import interfaces.Mozgasok;

public abstract class Jarmu implements Mozgasok {
	
	String Nev;
	String tipus;
	String marka;
	int fogyasztas;
	
	int uzemanyag=15;

	public Jarmu(String nev, String tipus,String marka,int fogyasztas) {
		this.Nev=nev;
		this.tipus=tipus;
		this.marka=marka;
		this.fogyasztas=fogyasztas;	
	}
	public float elore() {
		float egesz =uzemanyag/fogyasztas*100;
		float resz=uzemanyag%fogyasztas*10;
		return egesz+resz;					
	}
	public String hatra() {
		return null;
	}

	public String getNev() {
		return Nev;
	}

	public void setNev(String nev) {
		Nev = nev;
	}

	public String getTipus() {
		return tipus;
	}

	public void setTipus(String tipus) {
		this.tipus = tipus;
	}

	public String getMarka() {
		return marka;
	}

	public void setMarka(String marka) {
		this.marka = marka;
	}

	public int getFogyasztas() {
		return fogyasztas;
	}

	public void setFogyasztas(int fogyasztas) {
		this.fogyasztas = fogyasztas;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Nev == null) ? 0 : Nev.hashCode());
		result = prime * result + fogyasztas;
		result = prime * result + ((marka == null) ? 0 : marka.hashCode());
		result = prime * result + ((tipus == null) ? 0 : tipus.hashCode());
		result = prime * result + uzemanyag;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Jarmu other = (Jarmu) obj;
		if (Nev == null) {
			if (other.Nev != null)
				return false;
		} else if (!Nev.equals(other.Nev))
			return false;
		if (fogyasztas != other.fogyasztas)
			return false;
		if (marka == null) {
			if (other.marka != null)
				return false;
		} else if (!marka.equals(other.marka))
			return false;
		if (tipus == null) {
			if (other.tipus != null)
				return false;
		} else if (!tipus.equals(other.tipus))
			return false;
		if (uzemanyag != other.uzemanyag)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Nev=" + Nev + ", tipus=" + tipus + ", marka=" + marka + ", fogyasztas=" + fogyasztas  +"L"+" 15L-b�l megtett km: "+elore();
	}
	

}
